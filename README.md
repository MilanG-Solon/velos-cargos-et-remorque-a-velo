# Vélos cargos et remorque à vélo

**Objectifs :**
Rassembler plusieurs acteurs de l'écosystème vélo pour designer et auto-construire une remorque open source à Montréal. 

**Pourquoi des véhicules open source ?**
- Favoriser la réplicabilité 
- Favoriser l'adaptabilité et l'évolutivité pour répondre aux besoins
- Favoriser l'émergence d'un standard pour les attaches et donc l'interopérabilité des remorques
- Favoriser l'appropriation des véhicules


**Pourquoi de l'autoconstruction ?**
- Rendre accessible et conviviale la construction
- Favoriser l'appropriation des véhicules
- Limiter la dépendance à des chaines de production hors Québec 
- Limiter les coûts
- Favoriser les matériaux recyclés et le réemploi 


**Pourquoi une remorque ?**
- Utilisation par différents acteurs LocoMotion, CS, Livraison
- Besoin de standardisation, notamment du système d'attache, afin de permettre le paratge et l'utilisation par différents acteurs
- Conception et réalisation plus simple qu'un vélo ou un vélo cargo
- Coûts plus faibles
- Une première réalisation plus accessible est une manière d'entamer une dynamique autour des véhicules open source (vers des vélos open source)


**Critères** 
- Légéreté 
- Robustesse, résistance 
- Coûts
- Simplicité 
- Esthétique 

**Personnes potentielles**
- LocoMotion
- Participant.e.s LocoMotion
- Guillaume Fortin (CS) guillaumefortin2009@gmail.com
- Cyclistes solidaires
- CS Julien juliengab.go@gmail.com
- La Remise 
- Jules de la Remise
- Atelier la patente 
- FabMob Qc
- FabMobFr 
- Jalon - Tommy 
- ETS étudiant.e.s 
- Gregory Prescott <prescott.gregory@gmail.com>
- Anne Lautier - Membre de Solon, impliquée dans le début du projet LocoMotion, interessée par les remorques - <abou_63@hotmail.com> 

**Apport respectifs**
- La Remise - Réalisation accessible et matériaux disponibles 
- Cyclo NS -  Obtenir des pièces de seconde main (en particulier des roues de vélo pour enfant)
- LocoMotion - Interopérabilité et le partage
- CS - Cas d'usage, regard logistique 
- FabMob - Logique open source et logistique



**Déroulé**
1. Constituer le groupe 
2. Rencontre virtuelle 
3. Critères envies et recherches 
4. Hackathon et construction 

**Liste de ressources pour la construction de vélos et remorques a vélo. Axé sur l'open source et/ou l'autoconstruction.**

**Véhicules Open Source**
- FabMobFr - [Groupes de travail](https://forum.fabmob.io/c/nos-communs/VehOpenSource/15) pour des vehicules Open Source

**Vélos**
- [Vélo Libre](https://velolib.re/accueil/alpha) - Site en construction lien avec la FabMob
- [XYZ](http://www.xyzcargo.com/cycles/) - One seater - [Plan](http://www.n55.dk/MANUALS/SPACEFRAMEVEHICLES/DIY.pdf)
- Experimentation FabMobFr - [Lien](https://pad.fabmob.io/6dYKujlNSXKyE_7umcmTAg#) 

**Menaces**
- Assurance, problemes de RC en cas d'autoconstruction ? 
- 


**Remorques**
- Wike Diy Kit [Lien](https://wicycle.com/products/bike-trailers/diy) 
- [Projet](https://gitlab.com/gufor5/ateliers-fortin-bike-trailer-project) de Guillaume Fortin ( Cyclistes solidaires) 
- [Charette Bike](https://charrette.bike/), Remorque a 3 roues Lien (Plan non dispo actuellement)
- [Veloma](https://veloma.org/velo-cargo/remorques/)


